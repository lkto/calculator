# Prueba DXC


Micro servicio que permite realizar las operaciones matemáticas básicas a una cantidad x de números enviadas en un array lineal con el siguiente formato ```  ej: [2,3,4,5,4]  ``` 
Cabe resaltar que el array solo debe contener números de lo contrario dará error.

Para su funcionamiento se debe realizar una petición post a  ``` /calculator  ``` enviando el array por  ``` x-www-form-urlencoded  ``` , cuya key debe ser data.

si todo es correcto veremos un mensaje como este:


```sh
{
    "response": {
        "data": {
            "data": {
                "suma": 18,
                "resta": -14,
                "multiplicacion": 480,
                "divicion": 0.008333333333333333
            }
        },
        "error": ""
    }
}
```
Cabe destacar que para su creación utilizamos la librería NodeJs

# Como instalar
#### Requerimientos
1. Node version v10.16.3
2. JEST version 24.9.0

#### Pasos de instalacion
1. Clonar el repositorio desde SSH ``` git@gitlab.com:lkto/calculator.git ``` o desde HTTPS ``` https://gitlab.com/lkto/calculator.git ``` 
2. Ejecutar el comando  ``` npm install  ``` para instalar todas las dependencias de nodeJS
3. Luego ejecutar el comando   ``` npm run dev  ``` para levantar el servidor  ``` http://localhost:6677  ``` 
4. Para ejecutar los test unitarios y comprobar que todo funcione correctamente ejecutar   ```  npm t ``` 

Nota: Podrás utilizar el Micro servicio en la URL   ``` http://localhost:6677/calculator  ``` , recuerda seguir los pasos de funcionamiento explicados al inicio del documento.

# Que encontraras?

En este micro servicio a nivel de código encontraras dentro de la carpeta validations un archivo llamado ```arrayNumbers.ts``` , el cual es un servicio de validación que se encarga de decir si el array entrante solo contiene números. 
Usando la librería``` util ```de nodeJs nos facilitamos la consulta sobre el tipo de dato de cada archivo como se muestra a continuación

```sh
import { isNumber } from "util";

const isAllNumber = (data) =>  {
    let isAllNumber = true;
    if(typeof data.forEach === 'function') {
        data.forEach( (element) => {
            if(!isNumber(element)){
                isAllNumber = false;
            }
    
        });    
    }else{
        isAllNumber = false;
    }
    
    return isAllNumber;
}

export  default isAllNumber;
```

Además encontraras dentro de la carpeta service un archivo llamado ```calculator.ts``` en el cual se encuentra la lógica de nuestro micro servicio.

```sh
const sum = (data: any) => {
    let result = 0;
    data.forEach( (element: any, x: any) => {
        if(x == 0)
        {
            result = element;
        }else{
            result += element;
        }
        
    });
    return result;
}

const subtraction = (data: any) => {
    let result = 0;
    data.forEach( (element: any, x: any) => {
        if(x == 0)
        {
            result = element;
        }else{
            result -= element;
        }
        
    });
    return result;
}

const multiplication = (data: any) => {
    let result = 0;
    data.forEach( (element: any,  x: any) => {
        if(x == 0)
        {
            result = element;
        }else{
            result *= element;
        }
        
    });
    return result;
}

const division = (data: any) => {
    let result = 0;
    data.forEach( (element: any, x:any) => {
        if(x == 0)
        {
            result = element;
        }else{
            result /= element;
        }
    });
    return result;
}

export {sum, subtraction, multiplication , division};
```

estos servicios anteriores estas testeados con la librería jest, estos test se encuentran en la carpeta test y podrás ejecutarlos usando el comando : ```  npm t  ```

# Despliegue

Para desplegar nuestra aplicación necesitaremos un archivo llamado .gitlab-ci.yml , para este proyecto utilizamos el siguiente :

```sh

install_modules:
  stage: build
  script:
    - npm install
  artifacts:
    paths:
        - node_modules/
 
testing:
  stage: test
  script: npm t

production:
  type: deploy
  stage: deploy
  image: ruby:latest
  script:
    - apt-get update -qy
    - apt-get install -y ruby-dev
    - gem install dpl
    - dpl --provider=heroku --app=calculator-node --api-key=$calculator_key_heroku
  only:
    - master

```
# Maintainers

#### Jesson Ember Bejarano Mosquera 
- ingeniero.ember.bejarano@gmail.com
- [website](https://ember.sigtics.org/)
- [@lkto](https://gitlab.com/lkto)

